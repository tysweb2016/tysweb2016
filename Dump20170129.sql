CREATE DATABASE  IF NOT EXISTS `listadelacompra` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `listadelacompra`;
-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: listadelacompra
-- ------------------------------------------------------
-- Server version	5.7.16-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `invitacion`
--

DROP TABLE IF EXISTS `invitacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invitacion` (
  `token` int(11) NOT NULL AUTO_INCREMENT,
  `emailInvitador` varchar(100) NOT NULL,
  `emailInvitado` varchar(100) NOT NULL,
  `idLista` int(11) NOT NULL,
  PRIMARY KEY (`token`)
) ENGINE=InnoDB AUTO_INCREMENT=1320622585 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lista`
--

DROP TABLE IF EXISTS `lista`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lista` (
  `idLista` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  `emailCreador` varchar(100) NOT NULL,
  PRIMARY KEY (`idLista`),
  KEY `_idx` (`emailCreador`),
  CONSTRAINT `` FOREIGN KEY (`emailCreador`) REFERENCES `usuario` (`email`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `miembro`
--

DROP TABLE IF EXISTS `miembro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `miembro` (
  `email` varchar(100) NOT NULL,
  `idLista` int(11) NOT NULL,
  PRIMARY KEY (`email`,`idLista`),
  KEY `MiembroLista_idx` (`idLista`),
  CONSTRAINT `MiembroLista` FOREIGN KEY (`idLista`) REFERENCES `lista` (`idLista`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `MiembroUsuario` FOREIGN KEY (`email`) REFERENCES `usuario` (`email`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `producto`
--

DROP TABLE IF EXISTS `producto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `producto` (
  `idProducto` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  `cantidadDeseada` int(11) NOT NULL,
  `cantidadExistente` int(11) NOT NULL,
  `idLista` int(11) NOT NULL,
  `pathFoto` varchar(5000) DEFAULT NULL,
  PRIMARY KEY (`idProducto`),
  UNIQUE KEY `index3` (`nombre`,`idLista`),
  KEY `ProductoALista_idx` (`idLista`),
  CONSTRAINT `ProductoALista` FOREIGN KEY (`idLista`) REFERENCES `lista` (`idLista`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `pwd` varchar(45) NOT NULL,
  `pathFoto` varchar(5000) DEFAULT NULL,
  `idRedSocial` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping events for database 'listadelacompra'
--

--
-- Dumping routines for database 'listadelacompra'
--
/*!50003 DROP PROCEDURE IF EXISTS `insertarUsuario` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insertarUsuario`(in pEmail varchar(45), in pPwd varchar(40), out pExito varchar(200), out pIdUsuario int)
BEGIN
	DECLARE queryCrearUsuario VARCHAR(200);
	DECLARE queryAsignarPermisos VARCHAR(200);

	DECLARE EXIT HANDLER FOR 1062, 396
	begin
		ROLLBACK;
		set pExito='Error al registrar usuario. ¿Tal vez ya existe?';
	end;
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
	begin
		ROLLBACK;
		SET pExito='Error de acceso a la base de datos';
	END;
	DECLARE EXIT HANDLER FOR sqlwarning
	BEGIN
		ROLLBACK;
		set pExito='Warning de acceso a la base de datos';
	END;

	START TRANSACTION;

		
		Insert into Usuario (email) values (pEmail);

		/** En ultimoId guardamos ldc1, ldc2, etc. 
			LAST_INSERT_ID() devuelve el id autonumérico del último registro insertado **/
		set @ultimoId=concat('ldc', LAST_INSERT_ID());


		/************
		Suponiendo que ultimoId sea ldc120, queremos conseguir una instruccion de este estilo: 

		CREATE USER 'ldc120'@'%' IDENTIFIED BY 'pepe'

		...en donde pepe es la contraseña que se ha pasado como parámetro al procedimiento almacenado;
		************/

		Set @queryCrearUsuario = CONCAT('create user \'' , @ultimoId , '\'@\'%\' identified by \'', pPwd , '\';');

		PREPARE crearUsuario FROM @queryCrearUsuario;
		EXECUTE crearUsuario;


		/*********
		Abajo queremos una instruccion tipo grant SELECT,INSERT,DELETE,UPDATE,EXECUTE on listadelacompra.* to 'ldc120'@'%'
		**********/	
		Set @queryAsignarPermisos = CONCAT('grant SELECT,INSERT,DELETE,UPDATE,EXECUTE on juegos.* to \'', @ultimoId, '\'@\'%\';');

		PREPARE asignarPermisos FROM @queryAsignarPermisos;
		EXECUTE asignarPermisos;


		Set pExito='OK';
		set pIdUsuario=LAST_INSERT_ID();
	COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-01-29 22:44:33
