<%@ page language="java" contentType="application/json" pageEncoding="UTF-8"%>

<%@page import="edu.uclm.esi.listaDeLaCompra.domain.Lista"%>
<%@page import="edu.uclm.esi.listaDeLaCompra.domain.Usuario"%>
<%@page import="edu.uclm.esi.listaDeLaCompra.domain.Manager"%>
<%@page import="org.json.simple.JSONArray"%>
<%@page import="org.json.simple.JSONObject"%>

<%
	Usuario usuario=(Usuario) session.getAttribute("usuario");
	
	JSONObject resultado=new JSONObject();
	if (usuario!=null) {
		try {
			int idLista=Integer.parseInt(request.getParameter("idLista"));
			JSONObject lista=Manager.get().getLista(idLista);
			resultado.put("tipo", "OK");
			resultado.put("lista", lista);
		}
		catch (Exception e) {
			resultado.put("tipo", "ERROR");
			resultado.put("texto", e.getMessage());
		}
	} else {
		resultado.put("tipo", "NO_EXISTE");
	}
	out.println(resultado);
%>