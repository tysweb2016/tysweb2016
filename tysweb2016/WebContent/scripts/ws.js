"use strict";

var url="ws://" + window.location.hostname + ":" + window.location.port + "/listaDeLaCompra/servidorWS";
var chat;

function conectarWebSocket() {
	url=url + "?idUsuario=" + usuario.id;
	chat=new WebSocket(url);

	chat.onopen = function() {
		document.getElementById("listening").innerHTML="Escuchando";
	}
	
	chat.onerror = function() {
		document.getElementById("listening").innerHTML="Sin escuchar";
	}
	
	chat.onmessage = function(mensaje) {
		mensaje=JSON.parse(mensaje.data);
		var tipo=mensaje.tipo;
		if (tipo=="ACTUALIZACION_DE_LISTA") {
			var idLista=mensaje.idLista;		
			if (sessionStorage.idLista==idLista) {
				alert("Actualización en lista " + idLista); 
				var request = new XMLHttpRequest();
				request.open("post", "cargarLista.jsp");
				request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
				request.onreadystatechange = function() {
					if (request.readyState == 4) {
						var resultado = JSON.parse(request.responseText);
						if (resultado.tipo == "OK") {
							lista.idLista = idLista;
							lista.nombre = resultado.lista.nombre;
							lista.productos = resultado.lista.productos;
							lista.miembros = resultado.lista.miembros;
							lista.mostrar();
						} else if (resultado.tipo == "ERROR") {
							alert("Error al acceder al sistema: " + resultado.error);
						}
					}
				}
				request.send("idLista=" + idLista);
			}
		} else if (tipo=="ERROR") {
			alert(mensaje.texto);
		}
	}		
}