<%@ page language="java" contentType="application/json" pageEncoding="UTF-8"%>

<%@ page import="java.io.*,java.util.*, javax.servlet.*" %>
<%@ page import="javax.servlet.http.*" %>
<%@ page import="org.apache.commons.fileupload.*" %>
<%@ page import="org.apache.commons.fileupload.disk.*" %>
<%@ page import="org.apache.commons.fileupload.servlet.*" %>
<%@ page import="org.apache.commons.io.output.*" %>


<%@page import="edu.uclm.esi.listaDeLaCompra.domain.Usuario"%>
<%@page import="edu.uclm.esi.listaDeLaCompra.domain.Manager"%>
<%@page import="org.json.simple.parser.JSONParser"%>
<%@page import="org.json.simple.JSONObject"%>

<%
JSONObject resultado = new JSONObject();
Usuario usuario = (Usuario) session.getAttribute("usuario");
   File file ;
   int maxFileSize = 5000 * 1024;
   int maxMemSize = 5000 * 1024;
   String UPLOAD_DIRECTORY = "upload";
   String filePath = null;
   String fileName = null;
   //String filePath = "C:/Program Files/Apache Software Foundation/Tomcat 8.0/webapps/data/";

   String contentType = request.getContentType();
   if ((contentType.indexOf("multipart/form-data") >= 0)) {

      DiskFileItemFactory factory = new DiskFileItemFactory();
      factory.setSizeThreshold(maxMemSize);
      factory.setRepository(new File(System.getProperty("java.io.tmpdir")));
      ServletFileUpload upload = new ServletFileUpload(factory);
      upload.setSizeMax( maxFileSize );
   // constructs the directory path to store upload file
   			String uploadPath = getServletContext().getRealPath("")
   				+ File.separator + UPLOAD_DIRECTORY;
   			// creates the directory if it does not exist
   			File uploadDir = new File(uploadPath);
   			if (!uploadDir.exists()) {
   				uploadDir.mkdir();
   			}
      try{ 
         List fileItems = upload.parseRequest(request);
         Iterator i = fileItems.iterator();
         while ( i.hasNext () ) 
         {
            FileItem fi = (FileItem)i.next();
            if ( !fi.isFormField () )  {
            	fileName = new File(fi.getName()).getName();
				filePath = uploadPath + File.separator + fileName;
				File storeFile = new File(filePath);	
				// saves the file on disk
				fi.write(storeFile);;
                out.println("Uploaded Filename: " + filePath+ "<br>");
                out.println("H filePath: " + filePath+ "<br>");
                out.println("A Filename: " + fileName+ "<br>");
            }
         }
         Manager.get().subirFotoUsuario(usuario.getId(),fileName);
         
         resultado.put("tipo", "OK");
 		 resultado.put("fileName", fileName);
      }catch(Exception e) {
    	  resultado.put("tipo", "ERROR");
  			resultado.put("texto", e.getMessage());
      }
   }else{
	   resultado.put("RESULT", "WARNING");
	   resultado.put("MESSAGE", "No files uploaded");
   }

	out.println(resultado);
	response.sendRedirect("http://localhost:8080/listaDeLaCompra/index.html");
%>
