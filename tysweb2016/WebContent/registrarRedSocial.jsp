<%@ page language="java" contentType="application/json" pageEncoding="UTF-8"%>

<%@page import="edu.uclm.esi.listaDeLaCompra.domain.Manager"%>
<%@page import="edu.uclm.esi.listaDeLaCompra.domain.Usuario"%>
<%@page import="org.json.simple.parser.JSONParser"%>
<%@page import="org.json.simple.JSONObject"%>

<%
	String p=request.getParameter("p");
	JSONParser jsp=new JSONParser();
	JSONObject objeto=(JSONObject) jsp.parse(p);
	
	String email=objeto.get("email").toString();

	JSONObject resultado=new JSONObject();		
		try {
			Usuario usuario=Manager.get().registrarPorRedSocial(email);	
			resultado.put("tipo", "OK");
			session.setAttribute("usuario", usuario);
			Cookie cookie=new Cookie("idUsuario", ""+usuario.getId());
			cookie.setMaxAge(365*24*3600);
			response.addCookie(cookie);
		}
		catch (Exception e) {
			resultado.put("tipo", "ERROR");
			resultado.put("texto", e.getMessage());
		}
	out.println(resultado);
%>