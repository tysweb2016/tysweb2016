package edu.uclm.esi.listaDeLaCompra.domain;

import java.util.Hashtable;
import java.util.Vector;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import edu.uclm.esi.listaDeLaCompra.dao.DAOLista;
import edu.uclm.esi.listaDeLaCompra.dao.DAOUsuario;

public class Lista{
	private int id;
	private String nombre;
	private Usuario creador;
	private Vector<Producto> productos;
	private Hashtable<String, Usuario> miembros;

	public Lista(){
		this.productos = new Vector();
		this.miembros = new Hashtable();
	}

	public void comprar(int idProducto, int cantidad) throws Exception {
		Producto producto = (Producto)this.productos.get(idProducto);
		producto.comprar(cantidad);
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Vector<Producto> getProductos() {
		return this.productos;
	}

	public void setProductos(Vector<Producto> productos) {
		this.productos = productos;
	}

	public void setCreador(Usuario creador) {
		this.creador = creador;
	}

	public void add(Producto producto) {
		this.productos.add(producto);
	}

	public Usuario getCreador() {
		return this.creador;
	}

	public JSONObject toJSONObject(){
		JSONObject jso = new JSONObject();
		jso.put("id", Integer.valueOf(this.id));
		jso.put("nombre", this.nombre);
		JSONArray jsaProductos = new JSONArray();
		for (Producto producto : this.productos) {
			jsaProductos.add(producto.toJSONObject());
		}
		jso.put("productos", jsaProductos);
		JSONArray jsaMiembros = new JSONArray();
		Object eMiembros = this.miembros.elements();
		while (((java.util.Enumeration)eMiembros).hasMoreElements()) {
			Usuario usuario = (Usuario)((java.util.Enumeration)eMiembros).nextElement();
			jsaMiembros.add(usuario.toJSONObject());
		}
		jso.put("miembros", jsaMiembros);
		return jso;
	}

	public void add(Usuario usuario) {
		this.miembros.put(usuario.getEmail(), usuario);
	}
	
	public void abandonar(Usuario usuario, int idLista) throws Exception {
		boolean creador = DAOLista.abandonar(usuario, idLista);
		if(creador){
			this.miembros.clear();
			this.productos.clear();
		} else {
			this.miembros.remove(usuario.getEmail(), usuario);
		}
	}
}

