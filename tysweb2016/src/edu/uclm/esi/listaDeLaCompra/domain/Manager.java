package edu.uclm.esi.listaDeLaCompra.domain;

import edu.uclm.esi.listaDeLaCompra.dao.DAOLista;
import edu.uclm.esi.listaDeLaCompra.dao.DAOProducto;
import edu.uclm.esi.listaDeLaCompra.dao.DAOUsuario;

import java.io.File;
import java.util.Enumeration;
import java.util.Hashtable;
import javax.websocket.RemoteEndpoint.Basic;
import javax.websocket.Session;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class Manager
{
	private static Manager yo;
	private Hashtable<String, Usuario> usuariosPorEmail;
	private Hashtable<Integer, Usuario> usuariosPorId;

	private Manager()
	{
		this.usuariosPorEmail = new Hashtable();
		this.usuariosPorId = new Hashtable();
	}

	public static Manager get() {
		if (yo == null)
			yo = new Manager();
		return yo;
	}

	public Usuario registrar(String email, String pwd) throws Exception {
		Usuario usuario = new Usuario(email);
		usuario.insert(pwd);
		return usuario;
	}

	public Usuario conectar(int id) throws Exception {
		Usuario usuario = (Usuario)this.usuariosPorId.get(Integer.valueOf(id));
		if (usuario == null) {
			usuario = DAOUsuario.select(id);
			if (usuario == null)
				throw new Exception("No se encuentra ese usuario");
			usuario.loadListas();
			this.usuariosPorEmail.put(usuario.getEmail(), usuario);
			this.usuariosPorId.put(Integer.valueOf(id), usuario);
		}
		return usuario;
	}
	
	public Usuario registrarPorRedSocial(String email) throws Exception {
		Usuario usuario = DAOUsuario.registrarPorRedSocial(email);
		//usuario.registrarPorRedSocial();
		return usuario;
	}

	public Usuario identificar(String login, String pwd) throws Exception {
		Usuario usuario = DAOUsuario.select(login, pwd);
		usuario.loadListas();
		this.usuariosPorEmail.put(usuario.getEmail(), usuario);
		this.usuariosPorId.put(Integer.valueOf(usuario.getId()), usuario);
		return usuario;
	}

	public void comprar(Usuario emisor, int idLista, int idProducto) throws Exception{
		Producto producto = DAOProducto.select(idProducto);
		producto.comprar();
		JSONObject jso = new JSONObject();
		jso.put("tipo", "ACTUALIZACION_DE_LISTA");
		jso.put("idLista", Integer.valueOf(idLista));
		String msg = jso.toJSONString();
		broadcast(emisor, idLista, msg);
	}

	private void broadcast(Usuario emisor, int idLista, String msg) throws java.io.IOException {
		Enumeration<Usuario> usuarios = this.usuariosPorId.elements();

		while (usuarios.hasMoreElements()) {
			Usuario usuario = (Usuario)usuarios.nextElement();
			if ((usuario.getId() != emisor.getId()) && (usuario.getListas().get(Integer.valueOf(idLista)) != null)) {
				usuario.getSession().getBasicRemote().sendText(msg);
			}
		}
	}

	public void addProducto(Usuario emisor, int idLista, String nombre, int cantidadDeseada, int cantidadExistente) throws Exception
	{
		Producto producto = new Producto(nombre, cantidadDeseada, cantidadExistente);
		producto.insert(idLista);
		JSONObject jso = new JSONObject();
		jso.put("tipo", "ACTUALIZACION_DE_LISTA");
		jso.put("idLista", Integer.valueOf(idLista));
		String msg = jso.toJSONString();
		broadcast(emisor, idLista, msg);
	}
	

	public void guardarProducto(Usuario emisor, int idLista, int idProducto, String nombre, int cantidadDeseada, int cantidadExistente) throws Exception
	{
		Producto producto = DAOProducto.select(idProducto);
		producto.setNombre(nombre);
		producto.setCantidadDeseada(cantidadDeseada);
		producto.setCantidadExistente(cantidadExistente);
		producto.update();
		JSONObject jso = new JSONObject();
		jso.put("tipo", "ACTUALIZACION_DE_LISTA");
		jso.put("idLista", Integer.valueOf(idLista));
		String msg = jso.toJSONString();
		broadcast(emisor, idLista, msg);
	}

	public JSONArray getListasAsJSONArray(Usuario usuario) throws Exception
	{
		Hashtable<Integer, Lista> listas = usuario.loadListas();
		Enumeration<Lista> eListas = listas.elements();
		JSONArray result = new JSONArray();
		while (eListas.hasMoreElements()) {
			Lista lista = (Lista)eListas.nextElement();
			result.add(lista.toJSONObject());
		}
		return result;
	}

	public void crearLista(Usuario usuario, String nombreLista) throws Exception {
		Lista lista = new Lista();
		lista.setNombre(nombreLista);
		lista.setCreador(usuario);
		DAOLista.insert(lista);
		DAOUsuario.addToLista(usuario, lista.getId());
	}

	public JSONObject getLista(int idLista) throws Exception {
		Lista lista = DAOLista.select(idLista);
		return lista.toJSONObject();
	}

	public Usuario getUsuario(int id) {
		return (Usuario)this.usuariosPorId.get(Integer.valueOf(id));
	}

	public Hashtable<Integer, Usuario> getUsuarios() {
		return this.usuariosPorId;
	}
	
	public void eliminarProducto(Usuario emisor, int idLista, int idProducto) throws Exception{
		Producto producto = DAOProducto.select(idProducto);
		producto.eliminar();
		JSONObject jso = new JSONObject();
		jso.put("tipo", "ACTUALIZACION_DE_LISTA");
		jso.put("idLista", Integer.valueOf(idLista));
		String msg = jso.toJSONString();
		broadcast(emisor, idLista, msg);
	}
	
	public void eliminarUsuario(Usuario emisor, int idLista, int idUsuario) throws Exception{
		Usuario usuario = DAOUsuario.select(idUsuario);
		Lista lista = DAOLista.select(idLista);
		lista.abandonar(usuario, idLista);
		JSONObject jso = new JSONObject();
		jso.put("tipo", "ACTUALIZACION_DE_LISTA");
		jso.put("idLista", Integer.valueOf(idLista));
		String msg = jso.toJSONString();
		broadcast(emisor, idLista, msg);
	}
	
	public void subirFotoUsuario(int idUsuario, String filePath) throws Exception {
		DAOUsuario.insertFoto(idUsuario, filePath);
	}
	
	public void subirFotoProducto(int idProducto, String filePath) throws Exception {
		DAOProducto.insertFoto(idProducto, filePath);
	}
	
}


