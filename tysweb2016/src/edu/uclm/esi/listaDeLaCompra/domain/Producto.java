package edu.uclm.esi.listaDeLaCompra.domain;

import edu.uclm.esi.listaDeLaCompra.dao.DAOProducto;
import org.json.simple.JSONObject;

public class Producto
{
	private int idProducto;
	private String nombre;
	private int cantidadExistente;
	private int cantidadDeseada;
	private String pathFoto;

	public Producto() {}

	public Producto(String nombre, int cantidadDeseada, int cantidadExistente)
	{
		this.nombre = nombre;
		this.cantidadDeseada = cantidadDeseada;
		this.cantidadExistente = cantidadExistente;
	}

	public void comprar(int cantidad) throws Exception {
		this.cantidadExistente += cantidad;
		DAOProducto.update(this);
	}

	public int getIdProducto() {
		return this.idProducto;
	}

	public void setIdProducto(int idProducto) {
		this.idProducto = idProducto;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getCantidadExistente() {
		return this.cantidadExistente;
	}

	public void setCantidadExistente(int cantidadExistente) {
		this.cantidadExistente = cantidadExistente;
	}

	public int getCantidadDeseada() {
		return this.cantidadDeseada;
	}

	public void setCantidadDeseada(int cantidadDeseada) {
		this.cantidadDeseada = cantidadDeseada;
	}

	public JSONObject toJSONObject()
	{
		JSONObject jso = new JSONObject();
		jso.put("idProducto", Integer.valueOf(this.idProducto));
		jso.put("nombre", this.nombre);
		jso.put("cantidadExistente", Integer.valueOf(this.cantidadExistente));
		jso.put("cantidadDeseada", Integer.valueOf(this.cantidadDeseada));
		jso.put("pathFoto", this.pathFoto);
		return jso;
	}

	public void insert(int idLista) throws Exception {
		DAOProducto.insert(this, idLista);
	}

	public void comprar() throws Exception {
		setCantidadExistente(this.cantidadDeseada);
		DAOProducto.update(this);
	}

	public void update() throws Exception {
		DAOProducto.update(this);
	}
	
	public void eliminar() throws Exception {
		DAOProducto.delete(this);
	}
	
	public String getPathFoto() {
		return pathFoto;
	}

	public void setPathFoto(String pathFoto) {
		this.pathFoto = pathFoto;
	}
	
	
}

