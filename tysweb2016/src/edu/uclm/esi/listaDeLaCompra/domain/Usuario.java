package edu.uclm.esi.listaDeLaCompra.domain;

import edu.uclm.esi.listaDeLaCompra.dao.DAOProducto;
import edu.uclm.esi.listaDeLaCompra.dao.DAOUsuario;
import edu.uclm.esi.listaDeLaCompra.mail.Mail;
import java.util.Hashtable;
import java.util.Random;
import javax.websocket.RemoteEndpoint.Async;
import javax.websocket.Session;
import org.json.simple.JSONObject;

public class Usuario
{
	private int id;
	private String email;
	private Session session;
	private Hashtable<Integer, Lista> listas;
	private String pathFoto;

	public Usuario(String login)
	{
		this.email = login;
		this.listas = new Hashtable();
	}

	public void invitar(String emailInvitado, Lista lista) throws Exception {
		int token = Math.abs(new Random().nextInt());
		DAOUsuario.insertarInvitacion(token, this.email, emailInvitado, lista.getId());
		String texto = "Hola! Tu amigo " + this.email + " te quiere invitar a que te unas a la lista de la compra compartida " + lista.getId() + ".\n" + 
				"Haz clic aquí: http://localhost:8080/listaDeLaCompra/unirse.jsp?p=" + token;
		Mail.enviarMail(emailInvitado, "Invitación para unirte a la lista de la compra", texto);
	}

	public void insert(String pwd) throws Exception {
		DAOUsuario.insert(this, pwd);
	}

	public String getEmail() {
		return this.email;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getId() {
		return this.id;
	}

	public Hashtable<Integer, Lista> getListas() {
		return this.listas;
	}

	public Hashtable<Integer, Lista> loadListas() throws Exception {
		this.listas = DAOUsuario.getListas(this);
		return this.listas;
	}

	public static Usuario unirsePorToken(String token) throws Exception {
		String[] emailInvitado = new String[1];
		int idLista = DAOUsuario.getInvitacion(token, emailInvitado);
		if (idLista == -1)
			throw new Exception("No se encuentra el token. Quizás es muy antiguo y ha expirado");
		Usuario invitado = null;
		try {
			invitado = DAOUsuario.select(emailInvitado[0]);
		}
		catch (Exception e) {
			invitado = new Usuario(emailInvitado[0]);
			invitado.insert("");
		}
		DAOUsuario.addToLista(invitado, idLista);
		return invitado;
	}

	public JSONObject toJSONObject()
	{
		JSONObject result = new JSONObject();
		result.put("id", Integer.valueOf(this.id));
		result.put("email", this.email);
		result.put("pathFoto", this.pathFoto);
		return result;
	}

	public void setSession(Session session) {
		this.session = session;
	}

	public Session getSession() {
		return this.session;
	}

	public void broadcast(JSONObject msgSalida) {
		this.session.getAsyncRemote().sendText(msgSalida.toJSONString());
	}

	/*public void insertPorRedSocial(int idRedSocial, String pwd) throws Exception {
		DAOUsuario.insertPorRedSocial(this, idRedSocial, pwd);		
	}*/
	
	public String getPathFoto() {
		return pathFoto;
	}

	public void setPathFoto(String pathFoto) {
		this.pathFoto = pathFoto;
	}
	
	/*public void registrarPorRedSocial() throws Exception {
		DAOUsuario.registrarPorRedSocial(this);
	}*/
	
}
