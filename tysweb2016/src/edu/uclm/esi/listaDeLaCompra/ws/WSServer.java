package edu.uclm.esi.listaDeLaCompra.ws;

import java.util.Enumeration;
import java.util.List;
import java.util.Map;

import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import org.json.simple.JSONObject;

import edu.uclm.esi.listaDeLaCompra.domain.Manager;
import edu.uclm.esi.listaDeLaCompra.domain.Usuario;

@ServerEndpoint("/servidorWS")
public class WSServer{
  @OnOpen
  public void open(Session session){
    Map<String, List<String>> pars = session.getRequestParameterMap();
    int idUsuario = Integer.parseInt((String)((List)pars.get("idUsuario")).get(0));
    Usuario usuario = Manager.get().getUsuario(idUsuario);
    usuario.setSession(session);
    System.out.println("Ha llegado: " + usuario.getEmail());
  }
  
  @OnClose
  public void close(Session session){
    System.out.println("Se fue un pringao");
  } 
  
  private void broadcastActualizacionDeLista(int idLista, JSONObject msgSalida){
    Enumeration<Usuario> usuarios = Manager.get().getUsuarios().elements();
    while (usuarios.hasMoreElements()){
      Usuario usuario = (Usuario)usuarios.nextElement();
      try{
        if (usuario.loadListas().get(Integer.valueOf(idLista)) != null) {
          usuario.broadcast(msgSalida);
        }
      }
      catch (Exception e){
        e.printStackTrace();
      }
    }
  }
}