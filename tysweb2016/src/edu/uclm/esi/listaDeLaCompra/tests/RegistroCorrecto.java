package edu.uclm.esi.listaDeLaCompra.tests;

import java.util.regex.Pattern;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

import edu.uclm.esi.listaDeLaCompra.dao.Broker;

public class RegistroCorrecto {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
	System.setProperty("webdriver.chrome.driver", "/LibreriasJava/chromedriver.exe");   
	this.driver = new ChromeDriver();
    baseUrl = "http://localhost:8080/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Before
  public void vaciarUsuario()throws SQLException{
    Connection bd = Broker.get().getConnection();
    String sql = "DELETE FROM usuario where email = 'usuario@prueba.com'";
    PreparedStatement ps = bd.prepareStatement(sql);
    ps.executeUpdate();
    bd.close();
  }
  
  @Test
  public void testRegistroCorrecto() throws Exception {
    driver.get(baseUrl + "listaDeLaCompra/index.html");
    driver.findElement(By.id("cajaPwd1")).clear();
    driver.findElement(By.id("cajaPwd1")).sendKeys("prueba");
    driver.findElement(By.id("cajaPwd2")).clear();
    driver.findElement(By.id("cajaPwd2")).sendKeys("prueba");
    driver.findElement(By.id("cajaEmailRegistro")).clear();
    driver.findElement(By.id("cajaEmailRegistro")).sendKeys("usuario@prueba.com");
    driver.findElement(By.id("btnRegistrar")).click();
    Thread.sleep(2000);
    try {
      assertEquals("Escuchando", driver.findElement(By.id("listening")).getText());
    } catch (Error e) {
      verificationErrors.append(e.toString());
    }
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
