package edu.uclm.esi.listaDeLaCompra.tests;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class Invitar {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
	System.setProperty("webdriver.chrome.driver", "/LibreriasJava/chromedriver.exe");   
	this.driver = new ChromeDriver();
    baseUrl = "http://localhost:8080/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testInvitar() throws Exception {
	driver.get(baseUrl + "listaDeLaCompra/index.html");
	Thread.sleep(2000);
	driver.findElement(By.id("soyMiembro")).click();
	driver.findElement(By.id("cajaEmailLogin")).clear();
	driver.findElement(By.id("cajaEmailLogin")).sendKeys("admin");
	driver.findElement(By.id("cajaPwd")).clear();
	driver.findElement(By.id("cajaPwd")).sendKeys("admin");
	driver.findElement(By.id("btnEntrar")).click();
	Thread.sleep(2000);
    driver.findElement(By.linkText("listaPrueba")).click();
    driver.findElement(By.id("btnAddUser")).click();
    driver.findElement(By.id("emailInvitado")).clear();
    driver.findElement(By.id("emailInvitado")).sendKeys("prueba@prueba.com");
    driver.findElement(By.id("btnAddUserForm")).click();
    Thread.sleep(5000);
    try {
    	assertEquals("Se ha invitado a prueba@prueba", closeAlertAndGetItsText());
      } catch (Error e) {
        verificationErrors.append(e.toString());
      }
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
