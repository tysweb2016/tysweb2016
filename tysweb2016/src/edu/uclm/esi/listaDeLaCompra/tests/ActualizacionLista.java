package edu.uclm.esi.listaDeLaCompra.tests;

import java.util.regex.Pattern;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import edu.uclm.esi.listaDeLaCompra.dao.Broker;

public class ActualizacionLista {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
	System.setProperty("webdriver.chrome.driver", "/LibreriasJava/chromedriver.exe");   
	this.driver = new ChromeDriver();
    baseUrl = "http://localhost:8080/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }
  
  @Before
  public void anadirProducto()throws SQLException{
	  Connection bd = Broker.get().getConnection();
	  String sql = "Insert into Producto (idProducto, nombre, cantidadDeseada, cantidadExistente, idLista) "
	  		+ "values ('55', 'testWS', '10' , '8', '20');";
	  PreparedStatement ps = bd.prepareStatement(sql);
	  ps.executeUpdate();
	  bd.close();
  }

  @Test
  public void testActualizacionLista() throws Exception {
	driver.get(baseUrl + "listaDeLaCompra/index.html");
	Thread.sleep(2000);
	driver.findElement(By.id("soyMiembro")).click();
	driver.findElement(By.id("cajaEmailLogin")).clear();
	driver.findElement(By.id("cajaEmailLogin")).sendKeys("test1");
	driver.findElement(By.id("cajaPwd")).clear();
	driver.findElement(By.id("cajaPwd")).sendKeys("test1");
	driver.findElement(By.id("btnEntrar")).click();
	Thread.sleep(2000);
    driver.findElement(By.linkText("test")).click();
    Thread.sleep(2000);
    
    WebDriver driver2 = new ChromeDriver();
    Thread.sleep(2000);
    
    driver2.get(baseUrl + "listaDeLaCompra/index.html");
    Thread.sleep(20000);
	driver2.findElement(By.id("soyMiembro")).click();
	driver2.findElement(By.id("cajaEmailLogin")).clear();
	driver2.findElement(By.id("cajaEmailLogin")).sendKeys("test2");
	driver2.findElement(By.id("cajaPwd")).clear();
	driver2.findElement(By.id("cajaPwd")).sendKeys("test2");
	driver2.findElement(By.id("btnEntrar")).click();
	Thread.sleep(2000);
    driver2.findElement(By.linkText("test")).click();
    driver2.findElement(By.cssSelector("div.col-lg-1.col-xs-1 > button")).click();
    Thread.sleep(2000);
    driver2.quit();
    Thread.sleep(5000);
    
    assertEquals("Actualización en lista 20", closeAlertAndGetItsText());
    try {
      assertEquals("testWS\n10\n10", driver.findElement(By.id("panelListasBody")).getText());
    } catch (Error e) {
      verificationErrors.append(e.toString());
    }
  }
  
  @After
  public void eliminarProducto()throws SQLException{
	  Connection bd = Broker.get().getConnection();
	  String sql = "DELETE FROM producto where nombre = 'testWS'";
	  PreparedStatement ps = bd.prepareStatement(sql);
	  ps.executeUpdate();
	  bd.close();
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
