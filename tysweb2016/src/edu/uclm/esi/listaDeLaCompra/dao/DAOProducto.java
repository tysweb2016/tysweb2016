package edu.uclm.esi.listaDeLaCompra.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import edu.uclm.esi.listaDeLaCompra.domain.Lista;
import edu.uclm.esi.listaDeLaCompra.domain.Producto;


public class DAOProducto {
	
	public static Producto select(int id)throws Exception{
		Producto result = null;
		Connection bd = null;
		try{
			bd = Broker.get().getConnection();
			String sql = "Select nombre, cantidadDeseada, cantidadExistente, pathFoto from Producto where idProducto=?";
			PreparedStatement ps = bd.prepareStatement(sql);
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			if (rs.next()){
				result = new Producto();
				result.setIdProducto(id);
				result.setNombre(rs.getString(1));
				result.setCantidadDeseada(rs.getInt(2));
				result.setCantidadExistente(rs.getInt(3));
				result.setPathFoto(rs.getString(4));
			}
			else{
				throw new Exception("Producto dado de baja");
			}
			return result;
		}
		catch (Exception e){
			throw e;
		}
		finally{
			Broker.get().close(bd);
		}
	}
	
	public static void update(Producto producto) throws Exception {
		Connection bd = null;
		try {
			bd = Broker.get().getConnection();
			String sql = "Update Producto set nombre=?, cantidadDeseada=?, cantidadExistente=? where idProducto=?";
			
			PreparedStatement ps = (PreparedStatement)bd.prepareStatement(sql);
			ps.setString(1, producto.getNombre());
			ps.setInt(2, producto.getCantidadDeseada());
			ps.setInt(3, producto.getCantidadExistente());
			ps.setInt(4, producto.getIdProducto());
			ps.executeUpdate();
		} catch (Exception e) {
			throw e;
		} finally{
			Broker.get().close(bd);
		}
	}
	
	public static void delete(Producto producto) throws Exception {
		Connection bd = null;
		try {
			bd = Broker.get().getConnection();
			String sql = "DELETE from Producto where idProducto=?";
			
			PreparedStatement ps = (PreparedStatement)bd.prepareStatement(sql);
			
			ps.setInt(1,  producto.getIdProducto());
			ps.executeUpdate();
		} catch (Exception e) {
			throw e;
		} finally{
			Broker.get().close(bd);
		}
		
	}
	
	public static int insert(Producto producto, int idLista) throws Exception{
		Connection bd = null;
		int nuevoId = -1;
		try{
			bd = Broker.get().getConnection();
			String sql = "Insert into Producto (nombre, cantidadDeseada, cantidadExistente, idLista) values (?,?,?,?)";
			PreparedStatement ps = bd.prepareStatement(sql, 1);
			ps.setString(1, producto.getNombre());
			ps.setInt(2, producto.getCantidadDeseada());
			ps.setInt(3, producto.getCantidadExistente());
			ps.setInt(4, idLista);
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next())
			{
				nuevoId = rs.getInt(1);
				producto.setIdProducto(nuevoId);
			}
			return nuevoId;
		}
		catch (Exception e){
			throw e;
		}
		finally{
			Broker.get().close(bd);
		}
	}
	
	public static void insertFoto(int idProducto, String filePath) throws Exception {
		Connection bd = null;
		try {
			bd = Broker.get().getConnection();
			String sql = "Update Producto set pathFoto=? where idProducto=?";	
			PreparedStatement ps = (PreparedStatement)bd.prepareStatement(sql);
			ps.setString(1,  filePath);
			ps.setInt(2,  idProducto);
			ps.executeUpdate();
		} catch (Exception e) {
			throw e;
		} finally{
			Broker.get().close(bd);
		}	
	}
}
