package edu.uclm.esi.listaDeLaCompra.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import edu.uclm.esi.listaDeLaCompra.domain.Lista;
import edu.uclm.esi.listaDeLaCompra.domain.Producto;
import edu.uclm.esi.listaDeLaCompra.domain.Usuario;


public class DAOLista {
	public static Lista select(int idLista) throws Exception{
		Connection bd = null;
		Lista result = null;
		try {
			String sql = "Select nombre from Lista where idLista=?";

			bd= Broker.get().getConnection();
			PreparedStatement ps = bd.prepareStatement(sql);

			ps.setInt(1, idLista);
			ResultSet rs = ps.executeQuery();
			if(rs.next()){
				result = new Lista();
				result.setId(idLista);
				result.setNombre(rs.getString(1));
			}else{
				throw new Exception("La lista " + idLista + " no existe");
			}
		} catch(Exception e){
			e.printStackTrace();
		}

		finally{
			Broker.get().close(bd);
		}

		cargarProductos(result);
		cargarMiembros(result);
		return result;
	}

	private static void cargarProductos(Lista lista) throws Exception {
		Connection bd = null;

		try{
			String sql = "select idProducto, nombre, cantidadExistente, cantidadDeseada, pathFoto from Producto where idLista=?";
			bd = Broker.get().getConnection();
			PreparedStatement ps = bd.prepareStatement(sql);
			ps.setInt(1, lista.getId());
			ResultSet rs = ps.executeQuery();

			while(rs.next()){
				Producto producto = new Producto();
				producto.setIdProducto(rs.getInt(1));
				producto.setNombre(rs.getString(2));
				producto.setCantidadExistente(rs.getInt(3));
				producto.setCantidadDeseada(rs.getInt(4));
				producto.setPathFoto(rs.getString(5));
				lista.add(producto);
			}


		}catch(Exception e){
			throw e;
		}
		finally{
			Broker.get().close(bd);
		}
	}

	private static void cargarMiembros(Lista lista) throws Exception {
		Connection bd = null;

		try{
			String sql = "select email from Miembro where idLista=?";
			bd = Broker.get().getConnection();
			PreparedStatement ps = bd.prepareStatement(sql);
			ps.setInt(1, lista.getId());
			ResultSet rs = ps.executeQuery();

			while(rs.next()){
				Usuario usuario = new Usuario(rs.getString(1));
				lista.add(usuario);
			}


		}catch(Exception e){
			throw e;
		}
		finally{
			Broker.get().close(bd);
		}
	}

	public static int insert(Lista lista) throws Exception{
		Connection bd = null;
		int nuevoId = -1;
		try
		{
			bd = Broker.get().getConnection();
			String sql = "Insert into Lista (nombre, emailCreador) values (?, ?)";
			PreparedStatement ps = bd.prepareStatement(sql, 1);
			ps.setString(1, lista.getNombre());
			ps.setString(2, lista.getCreador().getEmail());
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next())
			{
				nuevoId = rs.getInt(1);
				lista.setId(nuevoId);
			}
			return nuevoId;
		}
		catch (Exception e)
		{
			throw e;
		}
		finally
		{
			Broker.get().close(bd);
		}
	}
	
	public static boolean abandonar(Usuario usuario, int idLista) throws Exception {
		Connection bd = null;
		try {
			bd = Broker.get().getConnection();
			String sql = "select emailCreador from Lista where idLista=?";
			PreparedStatement ps = (PreparedStatement)bd.prepareStatement(sql);
			ps.setInt(1, idLista);
			ResultSet rs = ps.executeQuery();
			rs.next();
			
			if(rs.getString(1).equals(usuario.getEmail())){
				sql = "DELETE from Lista where idLista=?";
				ps = (PreparedStatement)bd.prepareStatement(sql);
				ps.setInt(1, idLista);
				ps.executeUpdate();
				return true;
			} else {
				sql = "DELETE from Miembro where email=? AND idLista=?";
				ps = (PreparedStatement)bd.prepareStatement(sql);
				ps.setString(1, usuario.getEmail());
				ps.setInt(2, idLista);
				ps.executeUpdate();
				return false;
			}
		} catch (Exception e) {
			throw e;
		} finally{
			Broker.get().close(bd);
		}
	}
}
