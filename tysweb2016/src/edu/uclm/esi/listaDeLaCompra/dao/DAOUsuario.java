package edu.uclm.esi.listaDeLaCompra.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Hashtable;

import edu.uclm.esi.listaDeLaCompra.domain.Lista;
import edu.uclm.esi.listaDeLaCompra.domain.Producto;
import edu.uclm.esi.listaDeLaCompra.domain.Usuario;

public class DAOUsuario {
	public static Usuario select(int id) throws Exception {
		Usuario result = null;
		Connection bd = null;
		try {
			bd = Broker.get().getConnection();
			String sql = "Select email, pathFoto from Usuario where id=?";
			PreparedStatement ps = bd.prepareStatement(sql);
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				result = new Usuario(rs.getString(1));
				result.setId(id);
				result.setPathFoto(rs.getString(2));
			} else {
				throw new Exception("Usuario dado de baja");
			}
			return result;
		} catch (Exception e) {
			throw e;
		} finally {
			Broker.get().close(bd);
		}
	}

	public static Usuario select(String email) throws Exception {
		Usuario result = null;
		Connection bd = null;
		try {
			bd = Broker.get().getConnection();
			String sql = "Select id from Usuario where email=?";
			PreparedStatement ps = bd.prepareStatement(sql);
			ps.setString(1, email);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				result = new Usuario(email);
				result.setId(rs.getInt(1));
			} else {
				throw new Exception("Usuario dado de baja");
			}
			return result;
		} catch (Exception e) {
			throw e;
		} finally {
			Broker.get().close(bd);
		}
	}

	public static Usuario select(String login, String pwd) throws Exception {
		Usuario result = null;
		Connection bd = null;
		try {
			bd = Broker.get().getConnection();
			String sql = "Select * from Usuario where email=? and pwd=?";
			PreparedStatement ps = bd.prepareStatement(sql);
			ps.setString(1, login);
			ps.setString(2, pwd);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				result = new Usuario(login);
				result.setId(rs.getInt(1));
				result.setPathFoto(rs.getString(4));
			} else {
				throw new Exception("Error en tu email o password");
			}
			return result;
		} catch (Exception e) {
			throw e;
		} finally {
			Broker.get().close(bd);
		}
	}
	
	public static int insert(Usuario usuario, String pwd) throws Exception {
		Connection bd = null;
		int nuevoId = -1;
		try {
			bd = Broker.get().getConnection();
			String sql = "Insert into Usuario (email, pwd) values (?, ?)";
			PreparedStatement ps = bd.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, usuario.getEmail());
			ps.setString(2, pwd);
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next()) {
				nuevoId = rs.getInt(1);
				usuario.setId(nuevoId);
			}
			return nuevoId;
		} catch (Exception e) {
			throw e;
		} finally {
			Broker.get().close(bd);
		}
	}

	public static Hashtable<Integer, Lista> getListas(Usuario usuario) throws Exception {
		Hashtable<Integer, Lista> result = null;
		Connection bd = null;
		try {
			bd = Broker.get().getConnection();
			String sql = "Select Lista.idLista from Lista inner join Miembro "
					+ "on Miembro.idLista=Lista.idLista where Miembro.email=?";
			PreparedStatement ps = bd.prepareStatement(sql);
			ps.setString(1, usuario.getEmail());
			ResultSet rs = ps.executeQuery();
			result = new Hashtable<>();
			while (rs.next()) {
				int idLista = rs.getInt(1);
				Lista lista = DAOLista.select(idLista);
				result.put(idLista, lista);
			}
			return result;
		} catch (Exception e) {
			throw e;
		} finally {
			Broker.get().close(bd);
		}
	}
	
	public static void insertarInvitacion(int token, String emailInvitador, String emailInvitado, int idLista)
			throws Exception {
		Connection bd = null;
		try {
			bd = Broker.get().getConnection();
			String sql = "Insert into Invitacion (token, emailInvitador, emailInvitado, idLista) values (?, ?, ?, ?)";
			PreparedStatement ps = bd.prepareStatement(sql);
			ps.setInt(1, token);
			ps.setString(2, emailInvitador);
			ps.setString(3, emailInvitado);
			ps.setInt(4, idLista);
			ps.executeUpdate();
		} catch (Exception e) {
			throw e;
		} finally {
			Broker.get().close(bd);
		}
	}

	public static int getInvitacion(String token, String[] emailInvitado) throws Exception {
		Connection bd = null;
		int result = -1;
		try {
			bd = Broker.get().getConnection();
			String sql = "Select emailInvitado, idLista from Invitacion where token=?";
			PreparedStatement ps = bd.prepareStatement(sql);
			ps.setInt(1, Integer.parseInt(token));
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				emailInvitado[0] = rs.getString(1);
				result = rs.getInt(2);
			}
			return result;
		} catch (Exception e) {
			throw e;
		} finally {
			Broker.get().close(bd);
		}
	}

	public static void addToLista(Usuario usuario, int idLista) throws Exception {
		Connection bd = null;
		try {
			bd = Broker.get().getConnection();
			String sql = "Insert into Miembro (email, idLista) values (?, ?)";
			PreparedStatement ps = bd.prepareStatement(sql);
			ps.setString(1, usuario.getEmail());
			ps.setInt(2, idLista);
			ps.executeUpdate();
		} catch (Exception e) {
			throw e;
		} finally {
			Broker.get().close(bd);
		}
	}

	public static Usuario insertPorRedSocial(String email) throws Exception {
		Connection bd = null;
		int idRedSocial = 2;
		String pwd = "";
		int nuevoId = -1;
		Usuario usuario = null;
		try {
			bd = Broker.get().getConnection();
			String sql = "Insert into Usuario(email, idRedSocial, pwd) values (?, ?,?)";
			PreparedStatement ps = bd.prepareStatement(sql);
			ps.setString(1, email);
			ps.setInt(2, idRedSocial);
			ps.setString(3, pwd);
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next()) {
				nuevoId = rs.getInt(1);
				usuario.setId(nuevoId);
			}
			return usuario;
		} catch (Exception e) {
			throw e;
		} finally {
			Broker.get().close(bd);
		}	
	}
	
	public static void insertFoto(int idUsuario, String filePath) throws Exception {
		Connection bd = null;
		try {
			bd = Broker.get().getConnection();
			String sql = "Update Usuario set pathFoto=? where id=?";	
			PreparedStatement ps = (PreparedStatement)bd.prepareStatement(sql);
			ps.setString(1,  filePath);
			ps.setInt(2,  idUsuario);
			ps.executeUpdate();
		} catch (Exception e) {
			throw e;
		} finally{
			Broker.get().close(bd);
		}	
	}
	
	public static Usuario registrarPorRedSocial(String email) throws Exception {
		Usuario usuarioGoogle = null;
		try {
			usuarioGoogle = DAOUsuario.select(email);
		}
		catch (Exception e) {
			try {
				usuarioGoogle = DAOUsuario.insertPorRedSocial(email);
			}
			catch (Exception ex) {
				throw ex;
			}
		}
		return usuarioGoogle;
	}
	
	
	
	
}