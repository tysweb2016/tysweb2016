package edu.uclm.esi.listaDeLaCompra.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Vector;

public class Broker {
	private static Broker yo;
	private Connection conexionPrivilegiada;
	private Vector<Connection> conexionesLibres;
	private Vector<Connection> conexionesOcupadas;
	
	private Broker() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			String url = "jdbc:mysql://127.0.0.1:3306/listadelacompra?noAccessToProcedureBodies=true";
			this.conexionesLibres = new Vector<>(25);
			this.conexionesOcupadas = new Vector<>(25);
			for (int i = 0; i < 25; i++){
				Connection bd = DriverManager.getConnection(url, "root", "root");
				conexionesLibres.add(bd);
			}
			this.conexionPrivilegiada = DriverManager.getConnection(url, "adminLDC", "");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static Broker get() {
		if (yo == null)
			yo = new Broker();
		return yo;
	}

	public Connection getConnection() throws SQLException {
		if (conexionesLibres.size() == 0)
			throw new SQLException("No hay conexiones a la base de datos");
		Connection bd = conexionesLibres.remove(conexionesLibres.size() - 1);
		conexionesOcupadas.add(bd);
		return bd;
	}
	
	public synchronized void close(Connection bd) {
		conexionesOcupadas.remove(bd);
		conexionesLibres.add(bd);
	}
	
	public Connection getConnection(String user, String pwd) throws SQLException {
		String url = "jdbc:mysql://127.0.0.1:3306/listadelacompra?noAccessToProcedureBodies=true";
		Connection bd = DriverManager.getConnection(url, user, pwd);
		return null;
	}
}

