<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%
	//Origin solo va en la cabecera cuando se hace una peticion cruzada a un host diferente al propio
	String origen = request.getHeader("origin");
	response.addHeader("Access-Control-Allow-Origin", origen);
	response.addHeader("Access-Control-Allow-Credentials", "true");	

	String p = request.getParameter("p");
	Cookie cookie = new Cookie("producto", p);
	cookie.setMaxAge(3600*24*365*100);
	response.addCookie(cookie);
	
%>
